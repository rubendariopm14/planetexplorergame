﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/2Lambert" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader {
		//Tags { "LightMode"="ForwardBase" }
		pass{
		CGPROGRAM

		#pragma vertex vert
		#pragma fragment frag
		//#pragma Standard fullforwardshadows
		//#pragma target 3.0


		uniform float4 _Color;

		uniform float4 _LightColor0;
		//float4x4 _Object2World
		//float4x4 _World2Object
		//float4 _WorldSpaceLightPos0;

		struct vertexInput {
			float4 vertex: POSITION;
			float3 normal: NORMAL;
		};

		struct vertexOutput{
			float4 pos: SV_POSITION;
			float4 col: COLOR;
		};
		//vertex Function
		vertexOutput vert(vertexInput v){
			vertexOutput o;

			float3 normalDirection = normalize(mul(float4(v.normal,0), unity_WorldToObject)).xyz;
			float3 lightDirection;
			float3 atten = 1;

			lightDirection = normalize(_WorldSpaceLightPos0.xyz);
			float3 diffuseReflection = dot(normalDirection, lightDirection);

			o.col = float4( diffuseReflection, 1);
			o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
			return o;
		}

		float4 frag(vertexOutput i) : COLOR{
			return i.col;
		}

		ENDCG
		}
	}
	//FallBack "Diffuse"
}
