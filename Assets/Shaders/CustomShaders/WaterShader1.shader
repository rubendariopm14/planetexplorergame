﻿Shader "Custom/WaterShader1" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Scale ("Scale", float) = 1
		_ScaleNormal ("ScaleNormal" , float) = 1
		_Speed ("Speed", float) = 1
		_Frequency ("Frequency", float) = 1
		_MainTex ("Color (RGB) Alpha (A)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Alpha ("Alpha", Range(0,1)) = 1
	}
	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows Lambert alpha

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		#pragma vertex vert


		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};
		//struct VertOutput {
			
		//}
		float pi = 3.14;
		float _ScaleNormal;
		float _Scale;
		float _Speed;
		float _Frequency;
		half _Glossiness;
		half _Metallic;
		float _Alpha;
		fixed4 _Color;


		void vert (inout appdata_full v){
			float greatest = float(_Scale * _ScaleNormal * sin(pi/2));
			float vertOffset = ((v.vertex.x ));
			float yOffset = _Scale  * _ScaleNormal * sin(_Time.w * _Speed + vertOffset * _Frequency);
			v.vertex.y += yOffset;
			//v.normal = float3( ,1);
		}
		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = _Color.a;
		}
		ENDCG
	}
	//FallBack "Diffuse"
}

if(i<0)printf();