﻿Shader "Custom/Test1Shader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
	}

	SubShader {
		Pass{
			CGPROGRAM
			//pragmas
			#pragma vertex vert
			#pragma fragment frag

			//ud variables

			uniform float4 _Color;

			//baseInput structs
			struct vertexInput{
				float4 vertex : POSITION;
			};
			struct vertexOutput{
				float4 pos : SV_POSITION;
			};

			//vertex function
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}

			//frag function
			float4 frag(vertexOutput i) : COLOR{
				return _Color;
			}

			ENDCG
		}

	}
}
