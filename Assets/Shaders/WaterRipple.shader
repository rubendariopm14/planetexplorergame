﻿Shader "Custom/WaterRipple" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Scale ("Scale", float) = 0.001
		_Speed ("Speed", float) = 1
		_Frequency ("Frequency", float) = 500
	}
	SubShader {
		Tags { "RenderType"="Transparent"}
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		float _Scale, _Speed, _Frequency;
		//float _Amp1, _Amp2, _Amp3;
		//float _OffX1, _OffZ1, _OffX2, _OffZ2, _OffX3, _OffZ3,;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void vert (inout appdata_full v){
			half offsetvert = ((v.vertex.x*v.vertex.x) + (v.vertex.z*v.vertex.z) + (v.vertex.y*v.vertex.y));

			half value = _Scale * sin(_Time.w * _Speed + offsetvert * _Frequency);

			v.vertex *= value/100+1;
			v.normal *= value/100+1;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
