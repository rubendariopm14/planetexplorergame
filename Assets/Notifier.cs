﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Notifier : MonoBehaviour {
	public static Notifier instance;

	public Text notificationText;
	private string[] notStringArray;
	public const float holdTime = 2f;
	public const float releaseTime = 3f;

	void Awake(){
		instance = this;
	}

	void Start(){
		this.notify ("Welcome!",Color.green, 5);
	}

	private Coroutine coroutine;
	public void notify(string text, Color color, float holdTime){
		if (coroutine!=null)
			StopCoroutine (coroutine);
		notificationText.text = text;
		notificationText.gameObject.SetActive (true);
		color.a = 1;
		notificationText.color = color;
		coroutine = StartCoroutine (holdAndOut (notificationText, holdTime, releaseTime));
	}

	private IEnumerator holdAndOut(Text textInst, float holdTime, float releaseTime){
		yield return new WaitForSeconds (holdTime);
		while(textInst.color.a != 0){ 
			Color color = textInst.color;
			color.a = Mathf.MoveTowards (color.a, 0, Time.deltaTime *(Mathf.Abs(0-color.a)/1));
			textInst.color = color;
			yield return null;
		}
		notificationText.gameObject.SetActive (false);
	}
}
