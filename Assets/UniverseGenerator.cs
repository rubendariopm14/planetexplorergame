﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniverseGenerator : MonoBehaviour {

	public GameObject[] planetPrefabs;
	public float maxSolarSystemExtent = 50000;
	public int maxNumPlanets = 1;
	public float sunRadius = 200;

	private GameObject[] planetInstaces;
	void Start(){
		Random.InitState ((int)System.DateTime.Now.Ticks);
	}

	public void CreatePlanets(){
		planetInstaces = new GameObject[maxNumPlanets + 1];
		planetInstaces [0] = (GameObject)Instantiate (planetPrefabs [0], Vector3.zero, Quaternion.identity);	//Sun creation and save

		for (int i = 1;i < maxNumPlanets+1; i++) {
			float planetRadius = planetPrefabs [i].GetComponent<PlanetGenerator3> ().radius + 50;
			Vector3 randomPlanetPos = Random.insideUnitSphere * Random.Range (sunRadius + planetRadius, maxSolarSystemExtent - planetRadius);
			planetInstaces [i] = (GameObject)Instantiate (planetPrefabs [Random.Range(1,planetPrefabs.Length-1)], randomPlanetPos, Quaternion.identity);
		}
	}


}
