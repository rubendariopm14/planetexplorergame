﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LagMotion : MonoBehaviour {

	[Range(0f,1f)]
	public float smoothing = 0.5f;
	private Transform ownTrans;
	private Vector3 previousPos;
	private Quaternion previousRot;
	// Use this for initialization
	void Start () {
		ownTrans = gameObject.transform;
		updateValues ();
	}
	
	// Update is called once per frame
	void Update () {
		ownTrans.position = Vector3.Slerp (previousPos, ownTrans.position, smoothing);
		ownTrans.rotation = Quaternion.Slerp (previousRot, ownTrans.rotation, smoothing);
		updateValues ();
	}
	void updateValues(){
		previousPos = ownTrans.position;
		previousRot = ownTrans.rotation;
	}
}
