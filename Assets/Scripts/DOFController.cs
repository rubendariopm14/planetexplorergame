﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class DOFController : MonoBehaviour {

	//[RequireComponent (typeof(Camera))]
	public Transform objectToFocus;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (objectToFocus) {
			float distance = (gameObject.transform.position - objectToFocus.position).magnitude + 4;
			GetComponent<DepthOfField> ().focalLength = distance;
		}
	}
}
