﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuRotation : MonoBehaviour {

//	public Transform planetTransform;
	public Transform cameraTransform;
	public AnimationCurve movementCurve;
	public float rotationWidth = 5;
	public float rotationHeight = 5;
	private Resolution resolution;
	private Vector3 startingRotation;
	// Use this for initialization
	void Start () {
		if (cameraTransform)
			startingRotation = cameraTransform.localEulerAngles;
		resolution = Screen.currentResolution;
	}
	
	// Update is called once per frame
	void Update () {
		if (cameraTransform) {
			//Vector3 planetLook = Quaternion.LookRotation (planetTransform.position-cameraTransform.position).eulerAngles;
			Vector3 pos = Input.mousePosition;
			pos.y -= resolution.height / 2;
			pos.x -= resolution.width / 2;
			pos.y /= resolution.height;
			pos.x /= resolution.width;

			Vector3 rot = new Vector3 (-movementCurve.Evaluate (pos.y)*rotationHeight, movementCurve.Evaluate (pos.x)*rotationWidth, 0);

			cameraTransform.localRotation = Quaternion.Slerp(cameraTransform.localRotation,Quaternion.Euler(startingRotation + rot),Time.deltaTime*3);
		}
	}

	public void resolutionChanged(){
		
	}
}
