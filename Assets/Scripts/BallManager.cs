﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class BallManager : MonoBehaviour {

	public GameObject bullet;
	public GameObject bulletTank;
	public Text textObject;
	public int addingSpeed = 100;
	private int frameAddingSpeed;
	public int maxBullets = 8;
	private int actualbullCount = 0;
	private int bulletCount = 0;
	private GameObject[] bullets;
	private int count = 0;
	// Use this for initialization
	void Start () {
		//Debug.Log (innerBox.GetComponent<MeshFilter>().mesh.bounds.extents.ToString ());
		bullets = new GameObject[maxBullets];
		fillTank ();
	}
	
	// Update is called once per frame
	void Update () {
		if (textObject) {
			textObject.text = actualbullCount.ToString();
		}
		frameAddingSpeed = (int)(addingSpeed * Time.deltaTime);
		if (Input.GetKeyDown (KeyCode.E)) {
			fillTank ();
		}
		if (count < frameAddingSpeed)
			count++;
		else if (bulletCount > 0) {
			count = 0;
			bulletCount--;
			actualbullCount++;
			if (bulletTank) {
				bullets [actualbullCount - 1] = GameObject.Instantiate (bullet);
				bullets [actualbullCount - 1].transform.position = bulletTank.transform.position;
			}
		}
	}

	public bool addBullet(){
		bool returnVal = true;
		if (bulletCount + actualbullCount < maxBullets) {
			bulletCount++;
		} else
			returnVal = false;
		return returnVal;
	}

	public bool removeBullet(){
		bool returnVal = true;
		if (actualbullCount > 0) {
			actualbullCount--;
			GameObject.Destroy(bullets [actualbullCount]);
		} else	returnVal = false;
		return returnVal;
	}

	public void fillTank(){
		for (int i = 0; i < 40; i++) {
			addBullet ();
		}
	}
}
