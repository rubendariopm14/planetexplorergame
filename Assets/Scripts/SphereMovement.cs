﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Movement : MonoBehaviour {

	public enum MoveMode {Static, CameraBased};
	public MoveMode moveMode;
	public Text text;
	public Transform MCTransform;
	public float acceleration;
	public int maxAngularVelocity;

	bool good = false;
	//Transform myTransform;
	Rigidbody myBody;
	float forceX, forceY;


	void Start () {
		myBody = gameObject.GetComponent(typeof(Rigidbody)) as Rigidbody;
		myBody.maxAngularVelocity = maxAngularVelocity;

		//myTransform = gameObject.GetComponent (typeof(Transform)) as Transform;
		MCTransform = GameObject.Find ("MainCamera").GetComponent (typeof(Transform)) as Transform;
		text = GameObject.Find("Canvas").GetComponent (typeof(Text)) as Text;
	}
	

	void Update () {
		forceX = Input.GetAxis("Mouse X");
		forceY = Input.GetAxis ("Mouse Y");

		string toPrint = "Mouse X:";
		toPrint = toPrint + forceX.ToString();
		toPrint = toPrint + '\n';
		toPrint = toPrint + "Mouse Y:";
		toPrint = toPrint + forceY.ToString();
		text.text = toPrint;

		if (Input.GetKeyDown (KeyCode.Space)) {
			good = !good;
			Cursor.visible = !good;
		}

		movement ();
	}

	void movement(){
		//Vector3 correction = (MCTransform.position - myTransform.position).normalized;
		Vector3 addX = MCTransform.right * forceY;
		Vector3 addY = MCTransform.forward * -forceX;
		addX.y = 0;
		addY.y = 0;

		if (good) {
			myBody.AddTorque (addX * acceleration * Time.deltaTime, ForceMode.Impulse);
			myBody.AddTorque (addY * acceleration * Time.deltaTime, ForceMode.Impulse);
		}
	}
}
