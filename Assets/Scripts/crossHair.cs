﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crossHair : MonoBehaviour {

	public CameraControlAdva cameraScript;
	public float showCursorValue = 10f;
	private CanvasRenderer crOwn;
	// Use this for initialization
	void Start () {
		crOwn = GetComponent (typeof(CanvasRenderer)) as CanvasRenderer;
	}
	
	// Update is called once per frame
	void Update () {
		if (cameraScript) {
			if (cameraScript.distance <= showCursorValue)
				crOwn.SetAlpha (1);
			else
				crOwn.SetAlpha (0);
		}
	}
}
