﻿using UnityEngine;
using System.Collections;

public class Shooting : MonoBehaviour {

	public Rigidbody car;
	public GameObject bullet;
	public GameObject BMObject;
	public GameObject efxSystem;
	public ParticleAnimator turretAnimator;
	public Material lineMaterial;
	public float firepower = 10;
	public bool showPredictionLine = false;

	LineRenderer lr;

	// Use this for initialization
	void Start () {
		startLineRenderer ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0) && BMObject.GetComponent<BallManager>().removeBullet()) {
			Vector3 spawnLocation = gameObject.transform.position + gameObject.transform.up * 2.5f;
			GameObject bulletInstance = GameObject.Instantiate (bullet);
			bulletInstance.transform.position = spawnLocation;
			if (car)bulletInstance.GetComponent<Rigidbody> ().velocity = car.velocity;
			bulletInstance.GetComponent<Rigidbody> ().AddForce (gameObject.transform.up * firepower, ForceMode.Impulse);
			bulletInstance.GetComponent<DieTimer> ().enabled = true;

			if(turretAnimator)turretAnimator.beginBulletParticles ();

			GameObject efxInstance = GameObject.Instantiate (efxSystem);
			efxInstance.GetComponent<ParticleAnimator> ().beginBulletParticles ();
			efxInstance.transform.position = spawnLocation;
			efxInstance.transform.rotation = gameObject.transform.rotation;
			efxInstance.GetComponent<AudioSource> ().Play ();
			efxInstance.GetComponent<DieTimer> ().enabled = true;
		}
		updateLine ();
	}

	void startLineRenderer(){
		lr = gameObject.AddComponent (typeof(LineRenderer)) as LineRenderer;
		lr.material = lineMaterial;
		lr.endWidth = 0.1f;
		lr.endWidth = 0.1f;
		lr.startColor = Color.blue;
		lr.endColor = Color.red;
	}

	void updateLine(){
		
		if (showPredictionLine) {
			lr.enabled = true;
			lr.SetPositions (new Vector3[] {
				gameObject.transform.position + gameObject.transform.up * 2.5f,
				gameObject.transform.position + (gameObject.transform.up * 30)
			});
		} else
			lr.enabled = false;
		
	}
}
