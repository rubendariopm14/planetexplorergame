﻿using UnityEngine;
using System.Collections;

public class BallGenerator : MonoBehaviour {

	public int generationSpeed = 1000;

	private int count = 0;
	private int stepsToWait;
	private GameObject playerObj;
	//private BoxCollider myCollision;
	// Use this for initialization
	void Start () {
		//myCollision = GetComponent (typeof(BoxCollider)) as BoxCollider;
	}

	void OnTriggerEnter (Collider other){
		stepsToWait = (int)(generationSpeed * Time.deltaTime);

		bool player = false;
		if (other) {
			GameObject pl = other.transform.parent.gameObject as GameObject;
			if (pl.CompareTag ("Player"))
				player = true;

			for (int i = 0; i < 3; i++) {
				if (player)
					break;
				pl = pl.transform.parent.gameObject;
				if (pl.CompareTag ("Player"))
					player = true;
			}

			if (player) {
				playerObj = pl;


			}
		}
	}

	// Update is called once per frame
	void OnTriggerStay(Collider other){
		count++;
		if (count >= stepsToWait) {
			count = 0;
			if (playerObj != null) {
				playerObj.GetComponent<BallManager> ().addBullet ();
			}
		}
	}

	void OnTriggerExit (Collider other){
		playerObj = null;
	}

}
