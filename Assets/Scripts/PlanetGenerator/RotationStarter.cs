﻿using UnityEngine;
using System.Collections;

public class RotationStarter : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Rigidbody rb = GetComponent (typeof(Rigidbody)) as Rigidbody;
		rb.AddTorque (0f,0.03f,0f, ForceMode.VelocityChange);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
