﻿using UnityEngine;
using System.Collections;

public class PlanetGenerator : MonoBehaviour {

	const int mapChunkSize = 22;
	[Range(1,6)]
	public int levelOfDetail;
	public float noiseScale;

	public int toRaise;

	public float height;

	public float meshHeightMultiplier;
	public AnimationCurve meshHeightCurve;

	public int octaves;
	[Range(0,1)]
	public float persistance;
	public float lacunarity;

	public int seed;
	public Vector2 offset;
	public int vSubdiv;
	public int hSubdiv;
	public float radius;

	public bool autoUpdate;

	public TerrainType[] regions;

	public void GenerateMap() {
		// DeleteChildren
		//int childCount = gameObject.transform.childCount;
		/*int count = gameObject.transform.childCount;
		for (int i = 0; i<count;i++)
			GameObject.DestroyImmediate(gameObject.transform.GetChild (0).gameObject);

		float[,] noiseMap = Noise.GenerateNoiseMap (mapChunkSize, mapChunkSize, seed, noiseScale, octaves, persistance, lacunarity, getOffset(0));
		Color[] colourMap = new Color[mapChunkSize * mapChunkSize];
		for (int y = 0; y < mapChunkSize; y++) {
			for (int x = 0; x < mapChunkSize; x++) {
				float currentHeight = noiseMap [x, y];
				for (int i = 0; i < regions.Length; i++) {
					if (currentHeight <= regions [i].height) {
						colourMap [y * mapChunkSize + x] = regions [i].colour;
						break;
					}
				}
			}
		}*/


		//TextToFile.generate (noiseMap, toRaise);
		gameObject.GetComponent<MeshFilter>().mesh = SphereMeshGenerator.generateSphere(hSubdiv,vSubdiv,radius).CreateMesh();
		/*for (int part = 0; part < 6; part++) {
			float[,] noiseMap = Noise.GenerateNoiseMap (mapChunkSize, mapChunkSize, seed, noiseScale, octaves, persistance, lacunarity, getOffset(part));
			Color[] colourMap = new Color[mapChunkSize * mapChunkSize];
			for (int y = 0; y < mapChunkSize; y++) {
				for (int x = 0; x < mapChunkSize; x++) {
					float currentHeight = noiseMap [x, y];
					for (int i = 0; i < regions.Length; i++) {
						if (currentHeight <= regions [i].height) {
							colourMap [y * mapChunkSize + x] = regions [i].colour;
							break;
						}
					}
				}
			}
			GameObject obj = createObject (part);
			obj.transform.localPosition = getPosition (part);
			obj.transform.eulerAngles = getAngle(part);
			DrawMesh (obj, MeshGenerator.GenerateTerrainMesh (noiseMap, meshHeightMultiplier, meshHeightCurve, levelOfDetail, true, height), TextureGenerator.TextureFromColourMap (colourMap, mapChunkSize, mapChunkSize));
			MeshCollider ms = obj.AddComponent (typeof(MeshCollider)) as MeshCollider;
			ms.material = Resources.Load ("Sand.physicMaterial", typeof(PhysicMaterial)) as PhysicMaterial;
		}*/


		//}
	}

	GameObject createObject( int i){
		GameObject obj = new GameObject ("Mesh" + i.ToString());
		obj.transform.parent = this.transform;
		/*MeshFilter mf = */obj.AddComponent (typeof(MeshFilter));
		//mf.mesh = CreateMesh (100, 100);
		string materialName = "MaterialP_" + i.ToString();
		MeshRenderer rend = obj.AddComponent (typeof(MeshRenderer)) as MeshRenderer;
		//MaterialCreator.CreateMaterial (materialName);
		rend.sharedMaterial = Resources.Load (materialName, typeof(Material)) as Material;
		rend.sharedMaterial.SetFloat ("_Smoothness", 0f);
		return obj;
	}

	Vector3 getPosition(int id){
		Vector3 position;
		float interval = mapChunkSize / 2;
		if (id == 0)position = new Vector3 (0, 0, -interval);
		else if (id == 1)position = new Vector3 (interval, 0, 0);
		else if (id == 2)position = new Vector3 (0, 0, interval);
		else if (id == 3)position = new Vector3 (-interval, 0, 0);
		else if (id == 4)position = new Vector3 (0, interval, 0);
		else if (id == 5)position = new Vector3 (0, -interval, 0);
		else position = new Vector3 (0, 0, 0);
		return position;
	}
	Vector3 getAngle(int id){
		Vector3 rotation;
		float interval = 90;
		if (id == 0)rotation = new Vector3 (-interval, 0, 0);
		else if (id == 1)rotation = new Vector3 (-interval, 0, -interval);
		else if (id == 2)rotation = new Vector3 (-interval, 0, -interval*2);
		else if (id == 3)rotation = new Vector3 (-interval, 0, -interval*3);
		else if (id == 4)rotation = new Vector3 (0, 0, 0);
		else if (id == 5)rotation = new Vector3 (interval*2, 0, 0);
		else rotation = new Vector3 (0, 0, 0);
		return rotation;
	}
	Vector2 getOffset(int id){
		float interval = mapChunkSize / noiseScale;
		return new Vector2 (id * interval, 0);
	}

	void OnValidate() {
		if (lacunarity < 1) {
			lacunarity = 1;
		}
		if (octaves < 0) {
			octaves = 0;
		}
	}

	void DrawMesh(GameObject obj, MeshData meshData, Texture2D texture) {
		Mesh mesh = meshData.CreateMesh();
		MeshFilter mf = obj.GetComponent(typeof(MeshFilter)) as MeshFilter;
		mf.sharedMesh = mesh;
		Renderer rend = obj.GetComponent(typeof(Renderer)) as Renderer;
		rend.sharedMaterial.mainTexture = texture;
	}


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}
}
