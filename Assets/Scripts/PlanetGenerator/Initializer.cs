﻿using UnityEngine;
using System.Collections;

public class Initializer : MonoBehaviour {

	public GameObject planetGenObject;
	// Use this for initialization
	void Start () {
		planetGenObject.GetComponent <PlanetGenerator3>().GenerateMap();
	}

}
