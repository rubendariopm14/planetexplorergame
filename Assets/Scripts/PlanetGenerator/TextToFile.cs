﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
public class TextToFile : MonoBehaviour
{
	private const string FILE_NAME = @"C:\Users\ruben\Desktop\MyFile.txt";

	public static void generate(float[,] heightmap, int toRaise){
		if (File.Exists(FILE_NAME)) 
		{
			Debug.Log(FILE_NAME + " already exists.Overriting");
			File.Delete (FILE_NAME);
		}
		StreamWriter sr = File.CreateText(FILE_NAME);
		sr.WriteLine ("This is my file.");
		Mesh mesh = GameObject.Find("PlanetGenerator").GetComponent<MeshFilter>().mesh;

		int[] triangles = mesh.triangles; 
		Vector3[] vertices = mesh.vertices;
		/*sr.WriteLine ("Vertices");
		for(int i = 0; i < vertices.Length; i++){
			sr.Write (i.ToString () + ": ");
			sr.WriteLine(vertices[i].ToString());
		}
		sr.WriteLine ("\nTriangles");
		for(int i = 0; i < triangles.Length/3; i++){
			sr.Write (i.ToString () + ": ");
			sr.WriteLine(triangles[i].ToString() + ',' + triangles[i+1].ToString() + ',' + triangles[i+2].ToString());
		}*/

		//vertices [1] /= 1.5f;

		uint vertexIndex = 0;
		for (int y = 0; y < 22; y++) {
			for (int x = 0; x < 22; x++) {
				vertices [vertexIndex] *= heightmap [y, x]/2f + 1f;
				vertexIndex++;
			}
		}

		mesh.vertices = vertices;
		mesh.triangles = triangles;
		sr.Close();
	}
}