﻿using UnityEngine;
using System.Collections;

public class ParticleAnimator : MonoBehaviour {

	private Component[] ps;
	public AnimationCurve emmissionRate;
	public int emmisionRate = 20;
	public int emmisionDuration = 200;
	private int animationValue;
	void Start () {
		animationValue = emmisionDuration;
		//ps = new ParticleSystem[GetComponentsInChildren<ParticleSystem> ().Length+1] ;
		ps = GetComponentsInChildren<ParticleSystem> ();
		//ps[ps.Length-1] = GetComponent<ParticleSystem> ();
		gameObject.GetComponent<DieTimer> ().dieTimeDelay = emmisionDuration * Time.deltaTime * 10;
	}
	
	// Update is called once per frame
	void Update () {
		updateParticles ();
	}

	public void beginBulletParticles(){
		animationValue = 0;
	}

	void updateParticles(){
		if (animationValue <= emmisionDuration) {
			foreach (ParticleSystem system in ps) {
				ParticleSystem.EmissionModule em = system.emission;
				var main = system.main;
				float evaluation = emmissionRate.Evaluate ((float)animationValue / emmisionDuration);
				em.rateOverTime = evaluation * emmisionRate;
				main.startLifetime = Mathf.Clamp ((float)animationValue / emmisionDuration, 1f, 0.8f) * 1f;
				animationValue++;
			}
		}
	}
}
