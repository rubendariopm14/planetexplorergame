using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.SceneManagement;


public class ButtonCustom : MonoBehaviour{

	public RectTransform rt;
	public MaskableGraphic buttonIcon;
	public Color buttonHColor = Color.white;

//	[Space(5)]
//	[Header("If it has a background?")]
//	public MaskableGraphic background;
//	public Color normalColor = Color.gray;
//	public Color highlightedColor = Color.white;

//	public int type = 0;
//	public PointerPick pp;
//	public DropDownUI ddui;
	public bool isWorldSpace = false;
	public UnityEvent onClicked;
	private Color startColor;
	private Rect rectangle;

	void Start(){
		rt = gameObject.GetComponent<RectTransform> ();
		if (buttonIcon == null)
			buttonIcon = gameObject.GetComponent<MaskableGraphic> ();
		if(!rt)rt = gameObject.GetComponent<RectTransform> ();
		startColor = buttonIcon.color;
//		if (!image && !text)
//		startColor = buttonIcon.color;
	}
	void Update(){
		if (!isWorldSpace) {
			rectangle = rt.rect;
			Vector3 mPos = Input.mousePosition;
			if (mPos.x > rt.position.x - (rectangle.width / 2) && mPos.x < rt.position.x + (rectangle.width / 2)) {
				if (mPos.y > rt.position.y - (rectangle.height / 2) && mPos.y < rt.position.y + (rectangle.height / 2)) {
					handleHit ();
				} else
					buttonIcon.color = startColor;
			} else
				buttonIcon.color = startColor;
		} else if (hit) {
			handleHit ();
			hit = false;
		} else buttonIcon.color = startColor;
	}

	void handleHit(){
		buttonIcon.color = buttonHColor;
		if (Input.GetMouseButtonUp (0)) {
			Action ();
		}
	}

	void Action(){
//		if (type == 3)
//			ddui.toggle ();
//		if (type == 1 || type == 2)
//			pp.setBuildObject (gameObject.name, type);
//		else
			switch (gameObject.name) {
			case "Start":
				SceneManager.LoadScene (1);
				
				break;
			case "Exit":
				Application.Quit ();
				break;
			case "ExitToMenu":
				SceneManager.LoadScene (0);	
				break;
			case "Resume":
				CameraControlAdva.instance.togglePause ();
				break;
			default:
				onClicked.Invoke ();
				break;
			}
	}
	private bool hit = false;
	void beingHit(){
		hit = true;
	}


}
