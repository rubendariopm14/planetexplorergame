﻿using UnityEngine;
using System.Collections;

public class TimeManipulator : MonoBehaviour {

	[Range(0.01f,3f)]
	public float timeValue = 1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Time.timeScale = timeValue;
		//Time.fixedDeltaTime = timeValue;
	}
}
