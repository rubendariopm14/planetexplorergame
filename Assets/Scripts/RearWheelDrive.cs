﻿using UnityEngine;
using System.Collections;

public class RearWheelDrive : MonoBehaviour {

	private WheelCollider[] wheels;

	public Rigidbody car;
	public float maxSpeed = 70;
	public float maxAngle = 30;
	public float maxTorque = 300;
	[Range(1f,2f)]
	public float torqueDivider = 1.2f;
	public float brakeTorque = 300;
	public GameObject wheelShape;

	private bool brake = false;
	// here we find all the WheelColliders down in the hierarchy
	public void Start()
	{
		wheels = GetComponentsInChildren<WheelCollider>();

		for (int i = 0; i < wheels.Length; ++i) 
		{
			var wheel = wheels [i];
			//wheel.ConfigureVehicleSubsteps (10,30,35);

			// create wheel shapes only when needed
			if (wheelShape != null)
			{
				var ws = GameObject.Instantiate (wheelShape);
				ws.transform.parent = wheel.transform;
			}
		}
			
	}

	// this is a really simple approach to updating wheels
	// here we simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero
	// this helps us to figure our which wheels are front ones and which are rear
	public void Update()
	{
		float angle = maxAngle * Input.GetAxis("Horizontal");
		float torque = maxTorque * Input.GetAxis("Vertical");
		//if (torque < 0) torque *= 3;

		if (Input.GetKeyDown (KeyCode.Space)) {
			brake = !brake;
		}
		if (car.velocity.sqrMagnitude > maxSpeed) {
			torque = 0;
		}

		foreach (WheelCollider wheel in wheels)
		{
			// a simple car where front wheels steer while rear ones drive

			if (brake) {
				wheel.brakeTorque = brakeTorque;
			} else if (torque == 0) {
				wheel.motorTorque = 0f;
				wheel.brakeTorque = 1500f;
			} else {
				wheel.motorTorque = torque;
				wheel.brakeTorque = 0f;
			}
			
			
			if (wheel.transform.localPosition.z > 0)
				wheel.steerAngle = angle;
			

				
			
			// update visual wheels if any
			if (wheelShape) 
			{
				Quaternion q;
				Vector3 p;
				wheel.GetWorldPose (out p, out q);

				// assume that the only child of the wheelcollider is the wheel shape
				Transform shapeTransform = wheel.transform.GetChild (0);
				shapeTransform.position = p;
				shapeTransform.rotation = q;
			}

		}
	}
}
