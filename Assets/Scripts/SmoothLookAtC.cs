﻿using UnityEngine;
using System.Collections;

public class SmoothLookAtC : MonoBehaviour{

	public CameraControlAdva.ViewMode viewMode = CameraControlAdva.ViewMode.definitePos;
	public Transform target;
	public bool usePlanetOri = true;
	public Transform planet = null;
	public Transform otherOrientation = null;
	public float damping = 6.0f;
	public float YOffset = -0.2f;
	public bool smooth = true;

	private Vector3 definiteLocalRotation;
	void Update () {
		if (target) {

			Vector3 toSet = target.position + ((planet.position - target.position).normalized * YOffset);

			// Look at and dampen the rotation
			Quaternion rotation = Quaternion.identity;
			Quaternion rotationToSet;
			if (viewMode == CameraControlAdva.ViewMode.around) {
				if (usePlanetOri)
					rotation = Quaternion.LookRotation (toSet - transform.position, target.up);
				else
					rotation = Quaternion.LookRotation (toSet - transform.position, otherOrientation.up);

				if (smooth)
					rotationToSet = Quaternion.Slerp (transform.rotation, rotation, Time.deltaTime * damping);
				else
					rotationToSet = rotation;

				transform.rotation = rotationToSet;

			} else if (viewMode == CameraControlAdva.ViewMode.definitePos) {
				//if (transform.localRotation != definiteLocalRotation) {
					rotation = Quaternion.Euler(definiteLocalRotation);
					rotationToSet = Quaternion.Slerp (transform.localRotation, rotation, Time.deltaTime * damping);
					transform.localRotation = rotationToSet;
				//}
			}
		}
	}
	
	void Start () {
		if (target == null) {
			if (gameObject.name == "HealthCanvas")
				target = GameObject.FindGameObjectWithTag ("MainCamera").transform;
			else target = GameObject.FindGameObjectWithTag ("planet").transform;
		}
			
		if (planet == null)
			planet = GameObject.FindGameObjectWithTag ("planet").transform;

		definiteLocalRotation = transform.localEulerAngles;
	}
	
}