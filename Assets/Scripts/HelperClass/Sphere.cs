using UnityEngine;

public static class Sphere{
	public static Vector3 getByDegrees(float horizontalAngle, float verticalAngle){
		float x, y, z, radius;
		radius = Mathf.Sin (Mathf.PI * (verticalAngle / 180));

		y = -Mathf.Cos (Mathf.PI * (verticalAngle / 180));
		z = -Mathf.Cos (Mathf.PI * (horizontalAngle / 180));
		z *= radius;
		x = Mathf.Sqrt ((radius * radius) - (z * z));
		if (horizontalAngle > 180)
			x  = -x;
		return new Vector3 (x, y, z);
	}

	public static Vector2 getByPosition(Vector3 vector){
		vector = vector.normalized;
		float horizontalAngle, verticalAngle, radius;

		verticalAngle = (Mathf.Acos (-vector.y) / Mathf.PI) * 180f;
		radius = Mathf.Sin (Mathf.PI * (verticalAngle / 180));
		vector.z /= radius;
		horizontalAngle = (Mathf.Acos (-vector.z) / Mathf.PI) * 180f;
		if (vector.x < 0) {
			horizontalAngle = (180 - horizontalAngle) + 180;
		}
		return new Vector2 (horizontalAngle, verticalAngle);
	}

	public static Vector3 findVectorAndDistance(Vector2 a, Vector2 b){
		//float divisionFactor = Sphere.getDivisionFactor (a.y, b.y);
		float xdiff, ydiff;
		xdiff = a.x - b.x;
		if (Mathf.Abs (xdiff) >= 180) {
			if (xdiff >= 180)
				xdiff -= 180;
			else if (xdiff <= -180)
				xdiff += 180;
		}
		ydiff = b.y - a.y;
		Vector2 vector = new Vector2 (xdiff, ydiff).normalized;

		xdiff = Mathf.Abs (xdiff);
		ydiff = Mathf.Abs (xdiff);
		float distance = Mathf.Sqrt ((xdiff * xdiff) + (ydiff * ydiff));
		return new Vector3 (vector.x, vector.y, distance);
	}
		
	/*public static float getDivisionFactor(float A, float B){
		float C = (A + B) / 2;
		return Mathf.Sin ((C / 180) * Mathf.PI);
	}*/

	public static Vector3 getRotationToObject(Vector3 ownPos, Vector3 objectPos){
		Vector3 rotationVector = (objectPos - ownPos).normalized;
		Vector3 rotation = Quaternion.LookRotation(rotationVector).eulerAngles;
		return rotation;
	}
	public static Vector3 getRotationToObject(Vector3 ownPos, Vector3 objectPos, Vector3 upwards){
		Vector3 rotationVector = (objectPos - ownPos).normalized;
		Vector3 rotation = Quaternion.LookRotation(rotationVector, upwards).eulerAngles;
		return rotation;
	}

	public static Vector3[] createLine(Vector3 start, Vector3 end, int subdivisionNumber, float startHeight, float endHeight){
		subdivisionNumber += 2;
		Vector3[] vertsArray = new Vector3[subdivisionNumber];
		for (int i = 0; i < subdivisionNumber; i++) {
			float interpol = (float)i / (subdivisionNumber - 1);
			vertsArray [i] = Vector3.Slerp (start, end, (float)i / (subdivisionNumber - 1)) * ((endHeight * interpol) + (startHeight * ((float)1-interpol)));
		}
		return vertsArray;
	}
	/*public static float findAngleByVector2(Vector2 vector){
		float angle = (Mathf.Acos (-vector.y) / Mathf.PI) * 180f;

		if (vector.x < 0)
			angle = (180-angle) + 180;
		return angle;
	}*/
}