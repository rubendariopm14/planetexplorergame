﻿using UnityEngine;
using System.Collections;

public class Gravity : MonoBehaviour {

	private GameObject[] planets;
	[Range(0.3f,2f)]
	public float divisionFactor = 1;
	public float forceP;
	public float distance;

	Transform playerTransform;
	Rigidbody playerRigidBody;
	void Start(){
		planets = GameObject.FindGameObjectsWithTag("planet");
		playerTransform = gameObject.transform;
		playerRigidBody = gameObject.GetComponent (typeof(Rigidbody)) as Rigidbody;
	}
	// Update is called once per frame
	void Update () {

		foreach (GameObject planet in planets) {
			float multiplier = 1;
			if (planet.GetComponent (typeof(gravityMultiplier))) {
				multiplier = planet.GetComponent <gravityMultiplier> ().multiplier;
			}
			float dist = (planet.transform.position - playerTransform.position).magnitude;
			Vector3 direction = (planet.transform.position - playerTransform.position).normalized;
			distance = dist;

			float force = Time.deltaTime * 60 * multiplier * (planet.GetComponent<Rigidbody> ().mass / (200*(dist+50))) * divisionFactor;
			Vector3 forceVector = direction * force;
			//Debug.Log (forceVector.ToString ());
			forceP = force;
			playerRigidBody.AddForce (forceVector, ForceMode.Acceleration);
		}
	}
}
