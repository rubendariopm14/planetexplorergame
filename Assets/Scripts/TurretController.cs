﻿using UnityEngine;
using System.Collections;

public class TurretController : MonoBehaviour {

	public Camera Mcamera;
	public Transform hull;
	public Transform turret;

	[Range(0f,180f)]
	public float maxUpAngle = 70f;
	[Range(0f,180f)]
	public float maxDownAngle = 100f;
	[Range(0f,2f)]
	public float moveSpeed = 0.02f;

	private Vector3 hullInitialRotation;
	private Vector3 turretInitialRotation;
	// Use this for initialization
	void Start () {
		hullInitialRotation = hull.localEulerAngles;
		turretInitialRotation = turret.localEulerAngles;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 lookPoint = Mcamera.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, 300f));
		float beforHullEAngle = hull.localEulerAngles.y;
		float beforeTurretEAngle = turret.localEulerAngles.x;

		Quaternion lookRotation = Quaternion.LookRotation (lookPoint - turret.position, hull.up);
		hull.rotation = lookRotation;
		turret.rotation = lookRotation;

		hull.localEulerAngles = new Vector3(hullInitialRotation.x, hull.localEulerAngles.y, hullInitialRotation.z);

		float targetHullEuler = hull.localEulerAngles.y;

		//MoveSpeedFilter
			//Hull
		if ((beforHullEAngle + moveSpeed) < targetHullEuler) {
			if ((targetHullEuler - beforHullEAngle) > 180)hull.localEulerAngles = new Vector3(hull.localEulerAngles.x, beforHullEAngle - moveSpeed, hull.localEulerAngles.z);
			else hull.localEulerAngles = new Vector3(hull.localEulerAngles.x, beforHullEAngle + moveSpeed, hull.localEulerAngles.z);
		} else if ((beforHullEAngle - moveSpeed) > targetHullEuler) {
			if ((beforHullEAngle - targetHullEuler) > 180)hull.localEulerAngles = new Vector3(hull.localEulerAngles.x, beforHullEAngle + moveSpeed, hull.localEulerAngles.z);
			else hull.localEulerAngles = new Vector3(hull.localEulerAngles.x, beforHullEAngle - moveSpeed, hull.localEulerAngles.z);
		}
		turret.localEulerAngles = new Vector3(turret.localEulerAngles.x+90,hull.localEulerAngles.y,0);
		//**********************
	}
}
