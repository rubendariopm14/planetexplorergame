﻿using UnityEngine;
using System.Collections;

public class Reset : MonoBehaviour {

	public enum Mode {animateUpwards, resetToPosition};
	public Mode mode = Mode.animateUpwards;
	public Transform referenceTransform;
	public int animationDuration = 3000;
	public float upDistance = 10;
	public AnimationCurve animCurve;

	private Transform TMyself;
	private Vector3 startPosition;
	private Quaternion startQRotation;
	private Vector3 whereTo;
	private Vector3 startRotation;
	private Vector3 beginingRotation;
	private int animationLimit;
	private int animationNumber = 0;


	// Use this for initialization
	void Start () {
		TMyself = gameObject.transform;
		beginingRotation = TMyself.rotation.eulerAngles - (referenceTransform.position - TMyself.position);
		animationLimit = (animationDuration/ 1000) * 60;
		animationNumber = animationLimit;
		startPosition = TMyself.position;
		startQRotation = TMyself.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.R)) {
			switch (mode) {
			case Mode.animateUpwards:
				startAnimation (upDistance);
				break;
			case Mode.resetToPosition:
				TMyself.position = startPosition;
				TMyself.rotation = startQRotation;
				TMyself.GetComponent<Rigidbody> ().velocity = Vector3.zero;
				break;
			}
		}

		doAnimation ();
	}

	void startAnimation(float distance){
		startRotation = TMyself.eulerAngles.normalized;
		animationNumber = 0;
		startPosition = TMyself.position;
		whereTo = (TMyself.position - referenceTransform.position).normalized;
	}

	void doAnimation(){
		if (animationNumber < animationLimit) {
			animationNumber++;
			Vector3 toSetPosition = startPosition + ((whereTo * upDistance) * animCurve.Evaluate(((float)animationNumber / animationLimit)));
			Quaternion toSetRotation = Quaternion.Lerp(Quaternion.Euler(startRotation), Quaternion.FromToRotation (referenceTransform.position - TMyself.position, beginingRotation + (referenceTransform.position - TMyself.position)), animCurve.Evaluate ((float)animationNumber / animationLimit));
			TMyself.position = toSetPosition;
			TMyself.rotation = toSetRotation;
		}
	}
}
