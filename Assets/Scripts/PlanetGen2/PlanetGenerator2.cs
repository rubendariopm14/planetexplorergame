﻿using UnityEngine;
using System.Collections;

public class PlanetGenerator2 : MonoBehaviour {

	public int subCount;
	public float radius;
	public float noiseScale;
	[Range(0f,1.5f)]
	public float meshHeightMultiplier;

	public AnimationCurve meshHeightCurve;

	public int octaves;
	[Range(0,1)]
	public float persistance;
	public float lacunarity;

	public bool useFalloff = true;
	public int seed;
	public Vector2 offset;

	public bool autoUpdate;

	public TerrainType[] regions;

	float[,] falloffMap;

	void Awake(){
		falloffMap = FallOffGenerator.GenerateFalloffMap (subCount);
	}

	void OnPreRender(){
		GL.wireframe = true;
	}
	void OnPostRender(){
		GL.wireframe = false;
	}

	public void GenerateMap() {

		float[,] noiseMap = Noise.GenerateNoiseMap (subCount, subCount, seed, noiseScale, octaves, persistance, lacunarity, offset, Noise.NormalizeMode.Local);
		Color[] colourMap = new Color[subCount * subCount];
		for (int y = 0; y < subCount; y++) {
			for (int x = 0; x < subCount; x++) {
				if (useFalloff) {
					noiseMap [x, y] = Mathf.Clamp01 (noiseMap [x, y] - falloffMap [x, y]);
				}
				float currentHeight = noiseMap [x, y];
				for (int i = 0; i < regions.Length; i++) {
					if (currentHeight <= regions [i].height) {
						colourMap [y * subCount + x] = regions [i].colour;
						break;
					}
				}
			}
		}



		MeshData2 sphere = SphereMeshGenerator.generateSphere(subCount,subCount,radius);

		int lastVertices = subCount / 7;
		int vertCount = 0;
		for (int y = 0; y < subCount; y++) {
			for (int x = 0; x < subCount; x++) {
				float r = 1f;
				if (!useFalloff) {
					if (y < (lastVertices - 1) || y > subCount - lastVertices) {
						if (y > (subCount - lastVertices))
							r = 1 - ((float)(y - (subCount - (lastVertices + 1))) / lastVertices);
						else
							r = (float)y / (lastVertices - 1);
					}
				}

				sphere.vertices [vertCount] = sphere.vertices [vertCount] + sphere.vertices [vertCount] * meshHeightCurve.Evaluate (noiseMap [x, y]) * meshHeightMultiplier * r;
				vertCount++;
			}
		}


		Mesh mesh = sphere.CreateMesh ();
		gameObject.GetComponent<MeshFilter> ().mesh = mesh;
		gameObject.GetComponent<MeshCollider> ().sharedMesh = mesh;

		Texture2D texture = new Texture2D (subCount, subCount);
		texture.filterMode = FilterMode.Point;
		texture.wrapMode = TextureWrapMode.Clamp;
		texture.SetPixels (colourMap);
		texture.Apply ();

		gameObject.GetComponent<Renderer> ().material.mainTexture = texture;
	}
		
	void OnValidate() {
		if (lacunarity < 1) {
			lacunarity = 1;
		}
		if (octaves < 0) {
			octaves = 0;
		}

		falloffMap = FallOffGenerator.GenerateFalloffMap (subCount);
	}

	/*void DrawMesh(GameObject obj, MeshData meshData, Texture2D texture) {
		Mesh mesh = meshData.CreateMesh();
		MeshFilter mf = obj.GetComponent(typeof(MeshFilter)) as MeshFilter;
		mf.sharedMesh = mesh;
		Renderer rend = obj.GetComponent(typeof(Renderer)) as Renderer;
		rend.sharedMaterial.mainTexture = texture;
	}*/


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
