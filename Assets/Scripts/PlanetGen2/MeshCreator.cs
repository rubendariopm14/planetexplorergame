﻿using UnityEngine;
using System.Collections;

public class MeshCreator: MonoBehaviour {

	public float width = 1f;
	public float height = 1f;
	// Use this for initialization
	void Start () {
		MeshFilter mf = gameObject.GetComponent<MeshFilter> ();
		Mesh mesh = new Mesh();
		mf.mesh = mesh;

		//Vertices
		Vector3[] vertices = new Vector3[]{
			new Vector3 (-width/2,-height/2,0),
			new Vector3 (width/2,-height/2,0),
			new Vector3 (-width/2,height/2,0),
			new Vector3 (width/2,height/2,0)
		};

		//Triangles
		int[] triangles = new int[] {
			1,0,2,
			2,3,1
		};

		//Normals
		Vector3[] normals = new Vector3[] {
			-Vector3.forward,
			-Vector3.forward,
			-Vector3.forward,
			-Vector3.right
		};

		//UVs
		Vector2[] uvs = new Vector2[]{
			new Vector2(0,0),
			new Vector2(1,0),
			new Vector2(0,1),
			new Vector2(1,1)
		};

		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.normals = normals;
		mesh.uv = uvs;
	}



}
