﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CameraControlAdva : MonoBehaviour {

	public static CameraControlAdva instance;
	public enum ViewMode{around, definitePos};

	public ViewMode viewMode = ViewMode.definitePos;
	public GameObject planet;
	[HideInInspector]
	public GameObject toFollow;
	public GameObject disconnectButton;

	public float distance = 10f;
	[Range(-1f,1f)]
	public float yOffset = 0;
	public float lowestDist = 1f;
	public float highestDist = 60f;
	[Range(1f,50f)]
	public float smoothValue = 3f;

	private float upAngle = 110f;
	[Range(0.5f,3f)]
	public float mouseAcceleration = 1f;

	public float planetFOV = 80;
	public bool invert = false;

	//private GameObject spotlight;

	[HideInInspector]
	public PlayerController rotationToChange;
	[HideInInspector]
	public GameObject playerObject;
	public GameObject pauseMenu;

	float aroundAngle = 0f;
	public bool pause = false;
	public bool planetView = false;
	private GameObject previewObject;
	private Camera cam;
	private float normalFOV;
	//public VRInputModule vrInput;
	Transform myself;
	private float planetRadius = 1;
	private SmoothLookAtC lookAt;

	private Vector3 localStartingPos;
	private Transform startingParent;

	void Awake(){
		instance = this;
	}

	// Use this for initialization
	void Start () {
		startingParent = gameObject.transform.parent;
		localStartingPos = gameObject.transform.localPosition;

		//viewMode = ViewMode.around;
		lookAt = gameObject.GetComponent<SmoothLookAtC> ();
		cam = gameObject.GetComponent<Camera> ();
		normalFOV = cam.fieldOfView;
		if (toFollow && viewMode == ViewMode.around)
			gameObject.transform.parent = toFollow.transform;
		myself = gameObject.transform;
		//if (toFollowTransform != null)
			//myself.SetParent (toFollowTransform);

		cursorCheck ();
	}

	private Vector2 degreesOffset = Vector2.zero;
	// Update is called once per frame
	void Update () {

		/*if (spotlight){
			Ray ray = cam.ViewportPointToRay(new Vector3(0.5f,0.5f,0f));
			RaycastHit rayHit;
			if (Physics.Raycast(ray,out rayHit,50f)){
				spotlight.transform.rotation = Quaternion.Slerp(spotlight.transform.rotation, Quaternion.LookRotation ((rayHit.point - spotlight.transform.position).normalized),0.1f);
			}
		}*/
		/*if (vrInput && !Cursor.visible)
			vrInput.UpdateCursorPosition (new Vector2 (Screen.width/2, Screen.height/2));*/
		if (pauseMenu!=null)
			pauseMenuUpdate ();

		Vector3 beforePos = myself.position;
		if (viewMode == ViewMode.around) {
			if (toFollow) {
				if (!pause) {
				
					float mouseX = -Input.GetAxis ("Mouse X");
					float mouseY = -Input.GetAxis ("Mouse Y");
					float scroll = Input.GetAxis ("Mouse ScrollWheel");

					if (invert) {
						mouseX = -mouseX;
						mouseY = -mouseY;
					}

					if (planetView) {
						mouseX = -mouseX;
						mouseY = -mouseY;
					}

					aroundAngle += mouseX * mouseAcceleration;
					if ((upAngle + mouseY * mouseAcceleration) > 0f && (upAngle + mouseY * mouseAcceleration) < 180f)
						upAngle += mouseY * mouseAcceleration;

					if (aroundAngle < 0)
						aroundAngle += 360;
					if (aroundAngle > 360)
						aroundAngle -= 360;

					distance = distance + distance * (-scroll * 0.1f);

					if (distance < lowestDist)
						distance = lowestDist;
					else if (distance > highestDist)
						distance = highestDist;
				}

				if (planetView && planet != null) {
					Vector3 toSet = Sphere.getByDegrees (aroundAngle, upAngle) * distance * planetRadius * 0.2f;
					cam.fieldOfView = (cam.fieldOfView * 0.95f) + (planetFOV * 0.05f);
					myself.localPosition = toSet;
				} else {
					Vector3 toSet = Sphere.getByDegrees (aroundAngle, upAngle) * distance;
					toSet.y += yOffset * distance;
					myself.localPosition = toSet;

					cam.fieldOfView = (cam.fieldOfView * 0.95f) + (normalFOV * 0.05f);

					if (rotationToChange)
						rotationToChange.yOffset = -aroundAngle;
				}
			}
		} else {
			myself.localPosition = localStartingPos;
			//if (Vector3.Distance (myself.localPosition,localStartingPos) < 0.00001f)
			//	disableYourself ();
		} 
		float smoothLocal = smoothValue;
		if (planetView)smoothLocal*=0.5f;
		myself.position = Vector3.Lerp (beforePos, myself.position, Time.deltaTime * smoothLocal);

		//if (planet)
		//	togglePlanetView ();
	}

	private float lookYOffset;
	public void togglePlanetView(bool what){
			planetView = what;
			if (planetView) {
				myself.parent = planet.transform;
				lookAt.target = planet.transform;
				lookYOffset = lookAt.YOffset;
				lookAt.YOffset = 0;
			} else {
				myself.parent = toFollow.transform;
				lookAt.target = toFollow.transform;
				lookAt.YOffset = lookYOffset;
			}
		planetRadius = planet.GetComponent<PlanetGenerator3> ().radius;
	}

	public void changeFollow(GameObject gO){
		toFollow = gO;
		if (viewMode == ViewMode.around)
			gameObject.transform.parent = gO.transform;
	}

	public void toggleViewType(ViewMode mode){
		DebugConsole.Log ("ViewTypeToggled", "normal");
		viewMode = mode;
		if (viewMode == ViewMode.around) {
			//lookAt.viewMode = viewMode;
			gameObject.transform.parent = toFollow.transform;
		} else if (viewMode == ViewMode.definitePos) {
			//lookAt.viewMode = viewMode;
			lookAt.target = planet.transform;
			gameObject.transform.parent = startingParent;
		}
		cursorCheck ();
	}

	void pauseMenuUpdate(){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			togglePause ();
		}
	}
	public void togglePause(){
		pauseMenu.SetActive(!pauseMenu.activeSelf);
		if (pauseMenu.activeSelf)disconnectButton.SetActive (CustomNMUI.instance.connected);
		toggleComponents (!pauseMenu.activeSelf);
	}

	void toggleComponents(bool cm){
		if (playerObject!=null){
			playerObject.GetComponent<PlayerController> ().enabled = cm;
		}
			
		pause = !cm;

		if (cm && CustomNMUI.instance.connected) {
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
		} else {
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}
	}

	public void cursorCheck(){
		if (toFollow.tag == "Player" && viewMode == ViewMode.around) {
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
		} else if (viewMode == ViewMode.definitePos){
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}
	}

	/*
	void changePreview(){
		if (objectToShow != actualOTS) {
			Debug.Log ("Changed");
			objectToShow = actualOTS;
			if (previewObject != null)
				GameObject.Destroy (previewObject);
			previewObject = GameObject.Instantiate (objectToShow);
			previewObject.transform.localScale = new Vector3(64,64,64);
			Component[] meshRArray;
			meshRArray = previewObject.GetComponentsInChildren<MeshRenderer>();
			previewObject.GetComponent<Animation> ().enabled = false;
			if (previewMat)
			foreach (MeshRenderer mr in meshRArray) {
				mr.sharedMaterial = previewMat;
			}
		}
	}

	void updatePreview(){
		if (toFollow.CompareTag ("Player") && (objectToShow)) {
			
			if (GetComponent (typeof(Camera)) != null) {
				Camera camera = GetComponent (typeof(Camera)) as Camera;

				//Raycast and find the position of other object********************

				RaycastHit hit;
				Ray ray = camera.ViewportPointToRay(new Vector3(0.5f,0.5f,0));
				if (Physics.Raycast (ray, out hit, 300f)) {
					previewObject.SetActive (true);
					Vector3 showingPoint = hit.point;
					Vector3 showingDirection = hit.normal;
					//showingDirection.y -= 90;

					previewObject.transform.position = showingPoint;
					previewObject.transform.rotation = Quaternion.LookRotation (showingDirection, toFollowTransform.up);
					Transform childTransform = previewObject.transform.GetChild (0);
					Vector3 localAngles = childTransform.localEulerAngles;
					localAngles.x -= 90;
					childTransform.localEulerAngles = localAngles;
				} else previewObject.SetActive(false);
			}
		}
	}*/
}
