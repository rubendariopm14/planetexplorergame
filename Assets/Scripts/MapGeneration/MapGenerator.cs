﻿using UnityEngine;
using System.Collections;

public class MapGenerator : MonoBehaviour {


	public enum DrawMode {Plane, Sphere};
	public float radius = 5;
	public DrawMode drawMode = DrawMode.Plane;

	const int mapChunkSize = 241;
	[Range(1,6)]
	public int levelOfDetail;
	public float noiseScale;

	public float meshHeightMultiplier;
	public AnimationCurve meshHeightCurve;

	public int octaves;
	[Range(0,1)]
	public float persistance;
	public float lacunarity;

	public int seed;
	public Vector2 offset;

	public bool autoUpdate;

	public TerrainType[] regions;

	private MeshFilter mf;

	public void GenerateMap() {

		float[,] noiseMap = Noise.GenerateNoiseMap (mapChunkSize, mapChunkSize, seed, noiseScale, octaves, persistance, lacunarity, offset, Noise.NormalizeMode.Local);

		Color[] colourMap = new Color[mapChunkSize * mapChunkSize];
		for (int y = 0; y < mapChunkSize; y++) {
			for (int x = 0; x < mapChunkSize; x++) {
				float currentHeight = noiseMap [x, y];
				for (int i = 0; i < regions.Length; i++) {
					if (currentHeight <= regions [i].height) {
						colourMap [y * mapChunkSize + x] = regions [i].colour;
						break;
					}
				}
			}
		}

		MapDisplay display = gameObject.GetComponent (typeof(MapDisplay)) as MapDisplay;
		if (drawMode == DrawMode.Plane) {
			display.DrawMesh (MeshGenerator.GenerateTerrainMesh (noiseMap, meshHeightMultiplier, meshHeightCurve, levelOfDetail, false, 0f), TextureGenerator.TextureFromColourMap (colourMap, mapChunkSize, mapChunkSize));
		} else if (drawMode == DrawMode.Sphere){
			MeshData meshData = MeshGenerator.GenerateTerrainMesh (noiseMap, meshHeightMultiplier, meshHeightCurve, levelOfDetail, false, 0f);
			for (int i = 0; i < meshData.vertices.Length; i++) {
				meshData.vertices[i].y += radius;
				//meshData.vertices [i] = meshData.vertices [i].normalized * radius;
			}
			mf.mesh = meshData.CreateMesh ();
		}
	}

	void OnValidate() {
		mf = gameObject.GetComponent (typeof(MeshFilter)) as MeshFilter;

		if (lacunarity < 1) {
			lacunarity = 1;
		}
		if (octaves < 0) {
			octaves = 0;
		}
	}
}

[System.Serializable]
public struct TerrainType {
	public string name;
	public float height;
	public Color colour;
}