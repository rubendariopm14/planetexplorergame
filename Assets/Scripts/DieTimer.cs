﻿using UnityEngine;
using System.Collections;

public class DieTimer : MonoBehaviour {

	public float dieTimeDelay = 0;

	private float timer = 0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		if (timer > dieTimeDelay)
			Destroy (gameObject);
	}
}
