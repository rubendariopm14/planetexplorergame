﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Communication: NetworkBehaviour{
	public static Communication instance;
	void Awake(){
		instance = this;
	}

	public void updateMapEverywhere(){
		MiniMapController.instance.UpdatePlayerCount ();
		StartCoroutine (waitThenUpdate ());
		RpcUpdateMap ();
	}

	[ClientRpc]
	public void RpcUpdateMap(){
		DebugConsole.Log ("ServerCalledForUpdate", "warning");
		MiniMapController.instance.UpdatePlayerCount ();
		if (!isServer)
			StartCoroutine (waitThenUpdate ());
	}

	[TargetRpc]
	public void TargetSendMessage(NetworkConnection conn, string message, Color color, float holdTime){
		Notifier.instance.notify (message, color, holdTime);
	}

	public void SyncStaff(NetworkConnection conn){
		DebugConsole.Log ("AboutToSendPositions");
		//DebugConsole.Log ("PosLenghth="+PlanetGenerator3.instance.planetInfo.generalObjectsInfo.Length);
		if (isServer)
			DebugConsole.Log ("I am the server");
		StartCoroutine(waitThenSend (conn, PlanetGenerator3.instance.planetInfo.generalObjectsInfo));
	}

	[TargetRpc]
	private void TargetSyncTrees(NetworkConnection conn, ObjectInfo[] generalObjectsInfoLocal, int chunckID){
		//DebugConsole.Log ("PositionsReceived");
		PlanetGenerator3.instance.SyncTrees (generalObjectsInfoLocal, chunckID);
	}

	[TargetRpc]
	public void TargetSyncSun(NetworkConnection conn, Quaternion rotation){
		GameObject.Find("Sun").transform.rotation = rotation;
	}

	private IEnumerator waitThenSend(NetworkConnection conn, ObjectInfo[][] generalObjectsInfoLocal){
		yield return new WaitForSeconds (3);
		DebugConsole.Log ("SendingTrees");
		TargetSyncSun (conn, GameObject.Find ("Sun").transform.rotation);
		for (int i = 0; i < generalObjectsInfoLocal.Length; i++) {
			TargetSyncTrees (conn, generalObjectsInfoLocal [i], i);
		}
	}
	/*
	[Command]
	public void CmdChangePlayerName(short controllerID, string name){
		for (int i = 0; i < NetworkInfo.instance.playerList.Count; i++) {
			if (NetworkInfo.instance.playerList [i].GetComponent<NetworkIdentity> ().playerControllerId == controllerID) {
				NetworkInfo.instance.playerList [i].GetComponent<PlayerController> ().setName = name;
				break;
			}
		}
	}

	[Command]
	public void CmdChangePlayerContinent(short controllerID, PlayerController.Continent continent){
		for (int i = 0; i < NetworkInfo.instance.playerList.Count; i++) {
			if (NetworkInfo.instance.playerList [i].GetComponent<NetworkIdentity> ().playerControllerId == controllerID) {
				NetworkInfo.instance.playerList [i].GetComponent<PlayerController> ().continent = continent;
				break;
			}
		}
	}
*/
	private IEnumerator waitThenUpdate(){
		yield return new WaitForSeconds (1);
		MiniMapController.instance.UpdatePlayerCount ();
	}
}