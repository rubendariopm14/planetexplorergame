﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetworkInfo : NetworkManager {
	public GameObject[] toEnableWhenNotConnected;
	public bool doesNotNetwork = false;
	private bool treesCreated = false;
	private string ipAddress;

	public static NetworkInfo instance;
	public List<GameObject> playerList;

	void Awake(){
		instance = this;
	}

	public override void OnClientConnect (NetworkConnection conn){
		DebugConsole.Log ("ClientConnected: " + conn.address, "normal");
		base.OnClientConnect (conn);
		if (GameObject.FindGameObjectWithTag("IPText")!=null)
			GameObject.FindGameObjectWithTag("IPText").GetComponent<Text>().text = "Your ip is:" + Network.player.ipAddress;
	}
	public override void OnClientDisconnect(NetworkConnection conn){
		DebugConsole.Log ("ClientDisconnected: " + conn.address, "normal");
		base.OnClientConnect (conn);
		CustomNMUI.instance.wakeUp ();
	}
	public override void OnServerConnect(NetworkConnection conn){
		DebugConsole.Log ("ServerConnected: " + conn.address, "normal");
		base.OnServerConnect (conn);
		if (conn.address != "localClient") {
			DebugConsole.Log ("Other Address, Sending OBJPOS");
			Communication.instance.SyncStaff (conn);
		}
	}

	public override void OnServerDisconnect(NetworkConnection conn){
		DebugConsole.Log ("ServerDisconnected: " + conn.address, "normal");
		base.OnServerDisconnect (conn);
		Communication.instance.updateMapEverywhere ();
	}
	public override void OnStartHost(){
		DebugConsole.Log ("HostStarted", "normal");
		base.OnStartHost ();
		if (!treesCreated) {
			PlanetGenerator3.instance.createTrees ();
			treesCreated = true;
		}
		playerList.Clear ();
	}
	public override void OnClientError (NetworkConnection conn, int errorCode){
		DebugConsole.Log ("ClientError: errorCode_" + errorCode.ToString(), "normal");
		base.OnClientError (conn, errorCode);
	}
	public override void OnDropConnection (bool success, string extendedInfo){
		if (success)
		DebugConsole.Log ("DroppingConnection: " + success, "normal");
		else DebugConsole.Log ("DroppingConnection: " + success, "error");
		base.OnDropConnection (success, extendedInfo);
		CustomNMUI.instance.wakeUp ();
	}

	public override void OnStopHost (){
		DebugConsole.Log ("HostStopped", "normal");
		GameObject mainCamera = GameObject.FindGameObjectWithTag ("MainCamera");
		CameraControlAdva cca = mainCamera.GetComponent<CameraControlAdva> ();
		SmoothLookAtC slac = mainCamera.GetComponent<SmoothLookAtC> ();
			cca.toggleViewType (CameraControlAdva.ViewMode.definitePos);
		cca.enabled = true;
		slac.enabled = true;
		GameObject pauseMenu = GameObject.FindGameObjectWithTag ("PauseMenu");
		if (pauseMenu!=null)
			pauseMenu.SetActive (false);

		base.OnStopHost ();
	}

	public override void OnServerAddPlayer (NetworkConnection conn, short playerControllerId){
		base.OnServerAddPlayer (conn, playerControllerId);
		DebugConsole.Log ("PlayerAdded: " + conn.address, "normal");
		Communication.instance.updateMapEverywhere ();
		GameObject[] activePlayers = GameObject.FindGameObjectsWithTag ("Player");
		foreach (GameObject playerObj in activePlayers) {
			if (!playerList.Contains (playerObj))
				playerList.Add (playerObj);
		}
	}
	public override void OnServerRemovePlayer (NetworkConnection conn, UnityEngine.Networking.PlayerController player){
		DebugConsole.Log ("PlayerRemoved: IP:" + conn.address, "normal");
		base.OnServerRemovePlayer (conn, player);
		Communication.instance.updateMapEverywhere ();
		playerList.Remove (player.gameObject);
	}
	public override void OnStopClient (){
		DebugConsole.Log ("ClientStopped", "normal");
		//GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraControlAdva> ().toggleViewType ();
		base.OnStopClient ();
	}
	public override void OnStopServer (){
		DebugConsole.Log ("ServerStopped", "normal");
		//GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraControlAdva> ().toggleViewType ();
		base.OnStopServer ();
		StartCoroutine (enabler ());
	}

	void showEnabled(){
		for (int i = 0; i < toEnableWhenNotConnected.Length; i++) {
			toEnableWhenNotConnected [i].SetActive (true);
		}
	}

	void Start(){
		playerList = new List<GameObject> ();
		showEnabled ();
		if (doesNotNetwork) {
			PlanetGenerator3.instance.createTrees ();
		}
	}

	private IEnumerator enabler(){
		yield return new WaitForEndOfFrame();
		showEnabled ();
	}
}

