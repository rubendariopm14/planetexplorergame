﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Events;

public class CustomNMUI : MonoBehaviour {

	public static CustomNMUI instance;

	public NetworkInfo networkInfo;
	public RectTransform playerList;
	public string playerName;
	public InputField nameIF;
	public InputField portIF;
	public InputField addressIF;
	public PlayerController.Continent continent;
	public bool connected = false;
	private bool client = false;

	private NetworkClient nc;
	//public UnityEvent<string> nameEvent;
	// Use this for initialization
	/*void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		
	}*/

	void Awake(){
		instance = this;
	}

	public void wakeUp(){
		this.gameObject.SetActive (true);
		connected = false;
	}

	public void startHost(){
		NetworkClient nc = networkInfo.StartHost ();
		this.gameObject.SetActive (false);
		Notifier.instance.notify ("HostCreated", Color.green, 1);
		client = false;
		connected = true;
	}

	public void disconnect(){
		networkInfo.StopHost ();
		Notifier.instance.notify ("Disconnected", Color.green, 1);
		this.gameObject.SetActive (true);
		connected = false;
	}

	public void joinGame(){
		nc = networkInfo.StartClient();
		StartCoroutine (onJoined ());
	}

	public void updateIP(){
		networkInfo.networkAddress = addressIF.text;
	}

	public void updatePort(){
		int portInt = 7777;
		int.TryParse (portIF.text, out portInt);
		networkInfo.networkPort = portInt;
	}

	public void updatePlayerName(){
		playerName = nameIF.text;
	}

	public void updateContinent(PlayerController.Continent lcont){
		continent = lcont;
	}

	private IEnumerator onJoined(){
		yield return new WaitForEndOfFrame ();
		yield return new WaitForEndOfFrame ();
		if (nc.connection.isConnected) {
			Notifier.instance.notify ("ConnectionSuccesful", Color.green, 1);
			client = true;
			connected = true;
			gameObject.SetActive (false);
		} else Notifier.instance.notify ("ConnectionUnsuccessful", Color.red, 1);
	}

	public void setContinet(int contInt){
		continent = (PlayerController.Continent)contInt;
	}


	public void sendNameToPlayer(){
		if (CameraControlAdva.instance.toFollow != null) {
			CameraControlAdva.instance.toFollow.GetComponent<PlayerController> ().setName (playerName);
		}
	}
}
/*
[System.Serializable]
public class stringEvent: UnityEvent<string>{
	
}*/