﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof (PlanetGenerator))]
public class PlanetGeneratorEditor : Editor {

	public override void OnInspectorGUI() {
		PlanetGenerator mapGen = (PlanetGenerator)target;

		if (DrawDefaultInspector ()) {
			if (mapGen.autoUpdate) {
				mapGen.GenerateMap ();
			}
		}

		if (GUILayout.Button ("Generate")) {
			mapGen.GenerateMap ();
		}
	}
}