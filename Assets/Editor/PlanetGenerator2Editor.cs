﻿using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor (typeof (PlanetGenerator2))]
public class PlanetGenerator2Editor : Editor {



	public override void OnInspectorGUI() {
		PlanetGenerator2 mapGen = (PlanetGenerator2)target;

		if (DrawDefaultInspector ()) {
			if (mapGen.autoUpdate) {
				mapGen.GenerateMap ();
			}
		}

		if (GUILayout.Button ("Generate")) {
			mapGen.GenerateMap ();
		}
	}

}