﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerController : NetworkBehaviour {

	public enum Continent{NorthAmerica, SouthAmerica, Europe, Africa, Antartica, Asia, Australia};

	[SyncVar]public Continent continent = Continent.NorthAmerica;

	public float yOffset = 0;
	public float xOffset = 0;
	public float zOffset = 0;
	public float moveForce = 5;
	public float jumpForce = 10;
	public float animSpeedDiv = 2;
	public float ratioOut;

	public float maxSpeed = 5;
	public Animator anim;
	public Rigidbody ownRigidBody;
	public Transform ownTransform;
	public Transform torsoTransform;
	public Transform bulletSpawn;

	public Text nameText;

	public float initialBulletSpeed = 30f;
	public float bulletLifetime = 10f;
	public int maxBulletAmmount = 20;
	private Transform cameraTransform;
	public GameObject bulletPrefab;
	//public Camera camera;
	private Transform planet;
	// Use this for initialization
	private float speed;
	bool check = true;
	[SyncVar]bool dead = false;
	[SyncVar(hook="onPlayerChangeName")]string playerName = "";

	private SpawningManager spawnManager = null;
	public GameObject healthBar = null;
	void Start(){
		if (isLocalPlayer) {
			spawn ();
		}
		
	}

	// Update is called once per frame
	void Update () {
		if (!isLocalPlayer)
			return;
		inputControl ();
		alignToPlanet ();
		fireHandler ();
	}

	[Client]
	public void spawn(){
		//disabling HealthBar
		if (healthBar != null)
			healthBar.SetActive (false);

		//getting SpawnManager
		spawnManager = GameObject.Find ("SpawningManager").GetComponent<SpawningManager> ();
		//spawnManager.resetMPool (maxBulletAmmount, gameObject.GetComponent<NetworkIdentity>().connectionToServer);

		//spawning
		List<SpawningInfo> availableSpawns = spawnManager.availableSpawns;
		int randomNumber = (int)(Random.value * availableSpawns.Count);
		planet = GameObject.FindGameObjectWithTag ("planet").transform;
		gameObject.transform.position = availableSpawns [randomNumber].position; 
		alignToPlanet ();
		gameObject.transform.position += (gameObject.transform.up * 5);

		//setting camera up
		GameObject cameraObj = GameObject.FindGameObjectWithTag ("MainCamera");
		cameraTransform = cameraObj.transform;

		CameraControlAdva CCA = cameraObj.GetComponent<CameraControlAdva> ();
		CCA.rotationToChange = this;
		CCA.planetView = false;
		CCA.changeFollow (gameObject);
		CCA.invert = false;
		CCA.yOffset = 1f;
		CCA.toggleViewType (CameraControlAdva.ViewMode.around);
		CCA.cursorCheck ();

		CmdChangeContinent(CustomNMUI.instance.continent);
		SmoothLookAtC slac = cameraObj.GetComponent<SmoothLookAtC> ();
		slac.target = gameObject.transform;

		waitToSetName ();
	}

	void inputControl(){
		

		float vertValue = Input.GetAxis ("Vertical");
		if (vertValue > 0) {
			if (check) {
				check = false;
			}
			moveFoward (moveForce);
		}
		RaycastHit rayHit;
		if (Physics.Raycast (ownTransform.position,-ownTransform.up, out rayHit, 0.8f)) {
			ownRigidBody.AddForce (Input.GetAxis ("Jump") * jumpForce * ownTransform.up);
		}

		speed = torsoTransform.InverseTransformDirection (ownRigidBody.velocity).z;
		speed /= animSpeedDiv;
		speed = Mathf.Clamp (speed, 0.5f, 2f);

		if (anim) {
			
			anim.SetFloat ("Speed", speed);

			if (vertValue > 0)
				anim.SetBool ("Foward", true);
			else
				anim.SetBool ("Foward", false);

			if (Input.GetAxis ("Sprint") > 0)
				anim.SetBool ("Running", true);
			else
				anim.SetBool ("Running", false);
		}
	}

	void moveFoward(float force){
		Vector3 pushDirection = torsoTransform.forward;
		float ratio = 0;

		Vector3 rayDir = Vector3.Lerp (pushDirection, -ownTransform.up, 0.75f);

		Ray ray = new Ray (ownTransform.position, rayDir);
		RaycastHit rayHit;
		if (Physics.Raycast (ray, out rayHit, 0.8f)) {
			ratio = (ownTransform.up - rayHit.normal).magnitude;
			ratioOut = ratio;
			ratio = Mathf.Clamp01 (ratio);
		}
		float maxLocalSpeed = maxSpeed;
		if (anim.GetBool ("Running"))
			maxLocalSpeed *= 1.7f;
		if (speed < maxLocalSpeed) {
			ownRigidBody.AddForce (pushDirection * (1 - ratio) * force * Time.deltaTime*60);
			ownRigidBody.AddForce (ownTransform.up * ratio * force * Time.deltaTime*60);
		}
	}

	void alignToPlanet(){
		Vector3 rotation = Quaternion.LookRotation((planet.position - gameObject.transform.position).normalized).eulerAngles;
		rotation.x -= 90;
		gameObject.transform.eulerAngles = rotation;
		Vector3 localAngles = new Vector3(0,yOffset,0);
		torsoTransform.localEulerAngles = localAngles;
	}

	void fireHandler(){
		if ( Input.GetMouseButtonUp(0) && !anim.GetBool ("Running")) {
			CmdFire (bulletSpawn.position, bulletSpawn.forward);
		}
	}

	[ClientRpc]
	void RpcPlaySound(float pitch){
		AudioSource audio = bulletSpawn.GetComponent<AudioSource> ();
		audio.pitch = pitch;
		audio.Play ();
	}

	[Command]
	void CmdFire(Vector3 position, Vector3 rotVector){
		// Set up bullet on server
		var bullet = (GameObject)Instantiate(bulletPrefab, position, Quaternion.identity);
		bullet.GetComponent<Rigidbody>().velocity = rotVector * initialBulletSpeed;

		// spawn bullet on client, custom spawn handler will be called
		NetworkServer.SpawnWithClientAuthority(bullet, connectionToClient);
		AudioSource audio = bulletSpawn.GetComponent<AudioSource> ();
		audio.pitch = Random.Range (0.8f, 1.8f);
		audio.Play ();
		RpcPlaySound (audio.pitch);

		// when the bullet is destroyed on the server it wil automatically be destroyed on clients
		StartCoroutine (Destroy (bullet, bulletLifetime));
	}


	public IEnumerator Destroy(GameObject gO, float timer)
	{
		yield return new WaitForSeconds (timer);
		if (gO!=null)
			CmdUnspawnObject (gO);
	}

	void OnCollisionEnter(Collision collision){
		if (!isLocalPlayer)
			return;
		GameObject collisionObject = collision.gameObject;
		if (collisionObject.tag == "Bullet" && !collisionObject.GetComponent<NetworkIdentity>().hasAuthority) {
			DebugConsole.Log ("tookDamage", "warning");
			gameObject.GetComponent<HealthController> ().CmdDamage (5f);

			CmdUnspawnObject(collisionObject);
		}
	}

	[Command]
	void CmdUnspawnObject(GameObject gO){
		GameObject.Destroy (gO);
		NetworkServer.UnSpawn (gO);
	}


	private void onPlayerChangeName(string name){
		playerName = name;

		if (isServer) {
			List<GameObject> players = new List<GameObject> ();
			players.AddRange (GameObject.FindGameObjectsWithTag ("Player"));

			if (players.Contains (this.gameObject))
				players.Remove (this.gameObject);

			bool foundPlayer = false;
			for (int i = 0; i < players.Count; i++) {
				if (players [i].GetComponent<PlayerController> ().playerName == name) {
					Communication.instance.TargetSendMessage (players [i].GetComponent<NetworkIdentity> ().connectionToClient, 
						"A player has already joined with your username, your player will be disabled until then", Color.yellow, 2);
					disableYourself (true);
					foundPlayer = true;
					break;
				}
			}
			if (!foundPlayer)
				disableYourself (false);
		}

		nameText.text = name;
	}

	[ServerCallback]
	private void disableYourself(bool what){
		RpcDisableYourself (!what);
		this.gameObject.SetActive (!what);
	}

	[ClientRpc]
	private void RpcDisableYourself(bool what){
		this.gameObject.SetActive (what);
	}

	private IEnumerator waitToSetName(){
		yield return new WaitForSeconds (2);
		setName(CustomNMUI.instance.playerName);
	}


	public void setName(string name){
		playerName = name;
		onPlayerChangeName (name);
	}

	[Command]
	public void CmdChangeName(string name){
		playerName = name;
	}

	[Command]
	public void CmdChangeContinent(Continent continentLocal){
		continent = continentLocal;
	}
}
