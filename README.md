# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

A game I made, which has taken about 3 months so far, 2months just for the planet generation, and it's not close to finished, but I have another project for the other game I'm making based on this one, in that other one I'm gonna be using Unity's transport layer, and handling all the packet sending and receiving myself, as well as generating a new star system as the player moves out of the current one, the idea is primarly making a server side in a computer which is gonna run the game even when no players are connected, then sending all of the planetary data when a new player connects, while syncing each players inputs throughout the gameplay.

There's the planet generation script which I will have to improve, so far what I've archived for every chunck to match the next one applying falloffs at every chunk of the planet which is not very nice, but works. 
Oh yeah and there's an LOD controller included in the PlanetGenrator3 which updates the mesh as the player gets closer, the distance calculations are handled in another thread so as to prevent freezes every time it checks.

### How do I get set up? ###

This is the source code for the game, you will need to have unity3d installed so that you can edit whatever you like, then build it and play.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Just me you could reach me at rubendariopm14@gmail.com